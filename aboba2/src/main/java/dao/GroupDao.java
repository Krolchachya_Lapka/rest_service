package dao;
import models.Gruppa;
import models.Student;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateSessionFactoryUtil;
import java.util.List;
public class GroupDao {
    public Gruppa findById(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<Gruppa> gruppas =  session.createQuery("From Gruppa where id=:id",Gruppa.class).setParameter("id",id).list();
        session.close();
        return gruppas.get(0);
    }
    public void save(Gruppa gruppa) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(gruppa);
        tx1.commit();
        session.close();
    }
    public void update(Gruppa gruppa) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(gruppa);
        tx1.commit();
        session.close();
    }
    public void delete(Gruppa gruppa) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(gruppa);
        tx1.commit();
        session.close();
    }
    public List<Gruppa> findAll() {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<Gruppa> gruppas =  session.createQuery("From Gruppa", Gruppa.class).list();
        session.close();
        return gruppas;
    }
    public List<Gruppa> sort_gruppa() {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<Gruppa> gruppas =  session.createQuery("From Gruppa ORDER BY id DESC ", Gruppa.class).list();
        session.close();
        return gruppas;
    }
    public List<Gruppa> HardQueryWhere(String str){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<Gruppa> gruppas =  session.createQuery("From Gruppa where faculty.name=:faculty",Gruppa.class).setParameter("faculty",str).list();
        session.close();
        return gruppas;
    }
    public List<Gruppa> HardQueryLike(String str){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<Gruppa> gruppas =  session.createQuery("From Gruppa where name LIKE :name",Gruppa.class).setParameter("name","%"+str+"%").list();
        session.close();
        return gruppas;
    }
}

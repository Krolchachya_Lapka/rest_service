package dao;
import models.Gruppa;
import models.Student;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateSessionFactoryUtil;
import java.util.List;
public class StudentDao {
    public Student findById(int id) {
        //return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Student.class, id);
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<Student> students =  session.createQuery("From Student where id=:id",Student.class).setParameter("id",id).list();
        session.close();
        return students.get(0);
    }
    public void save(Student student) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(student);
        tx1.commit();
        session.close();
    }
    public void update(Student student) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(student);
        tx1.commit();
        session.close();
    }
    public void delete(Student student) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(student);
        tx1.commit();
        session.close();
    }
    public List<Student> findAll() {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<Student> students =  session.createQuery("From Student",Student.class).list();
        session.close();
        return students;
    }
    public List<Student> sort_student() {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<Student> students =  session.createQuery("From Student ORDER BY full_name ", Student.class).list();
        session.close();
        return students;
    }
    public List<Student> HardQueryWhere(String str){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<Student> students =  session.createQuery("From Student where gruppa.name=:gruppa",Student.class).setParameter("gruppa",str).list();
        session.close();
        return students;
    }
    public List<Student> HardQueryLike(String str){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<Student> students =  session.createQuery("From Student where full_name LIKE :name",Student.class).setParameter("name","%"+str+"%").list();
        session.close();
        return students;
    }
}

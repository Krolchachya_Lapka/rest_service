package dao;
import models.Faculty;
import models.Gruppa;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateSessionFactoryUtil;
import java.util.List;
public class FacultyDao {
    public Faculty findById(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<Faculty> faculties =  session.createQuery("From Faculty where id=:id",Faculty.class).setParameter("id",id).list();
        session.close();
        return faculties.get(0);
    }
    public void save(Faculty faculty) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(faculty);
        tx1.commit();
        session.close();
    }
    public void update(Faculty faculty) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(faculty);
        tx1.commit();
        session.close();
    }
    public void delete(Faculty faculty) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(faculty);
        tx1.commit();
        session.close();
    }
    public List<Faculty> findAll() {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<Faculty> faculties =  session.createQuery("From Faculty",Faculty.class).list();
        session.close();
        return faculties;
    }
    public List<Faculty> sort_faculty() {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<Faculty> faculties =  session.createQuery("From Faculty ORDER BY name", Faculty.class).list();
        session.close();
        return faculties;
    }
    public List<Faculty> HardQueryLike(String str){
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<Faculty> faculties =  session.createQuery("From Faculty where name LIKE :name",Faculty.class).setParameter("name","%"+str+"%").list();
        session.close();
        return faculties;
    }
}

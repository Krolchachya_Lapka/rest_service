package com.example.aboba2;

import models.Faculty;
import models.Gruppa;
import models.Student;
import services.FacultyService;
import services.GroupService;
import services.StudentService;

import javax.ws.rs.*;
import java.util.List;

@Path("/hi")
public class StudentRes {
    public static int count=5;
    @POST
    @Path("/update-stud")
    @Produces("text/html")
    public String update_stud(@FormParam("id_stud") int id_stud,@FormParam("name_stud") String name_stud,@FormParam("id_gruppa") int id_gruppa){ //обновление студента
        StudentService studentService = new StudentService();
        Student student=studentService.findStudent(id_stud);
        student.setFull_name(name_stud);
        GroupService groupService = new GroupService();
        Gruppa gruppa = groupService.findGroup(id_gruppa);
        student.setGroup(gruppa);
        studentService.updateStudent(student);
        return "DELO SDELANO";
    }
    @POST
    @Path("/delete-stud")
    @Produces("text/html")
    public String delete_stud(@FormParam("id_stud") int id_stud){ // удаление студента
        StudentService studentService = new StudentService();
        Student student=studentService.findStudent(id_stud);
        studentService.deleteStudent(student);
        return "DELO SDELANO";
    }
    @POST
    @Path("/add-stud")
    @Produces("text/html")
    public String add_stud(@FormParam("name_stud") String name_stud, @FormParam("id_gruppa") int id_gruppa){ //добавление студента
        StudentService studentService = new StudentService();
        GroupService groupService = new GroupService();
        Student student=new Student(name_stud);
        Gruppa gruppa = groupService.findGroup(id_gruppa);
        student.setGroup(gruppa);
        studentService.saveStudent(student);
        return "DELO SDELANO";
    }
    @POST
    @Path("/hardquery")
    @Produces("text/html")
    public String hardquery(@FormParam("name_gruppa") String name_gruppa){ //поиск студентов по группе
        StudentService studentService = new StudentService();
        List<Student> studentList = studentService.HardQueryWhere(name_gruppa);
        String str = "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Student</title>\n" +
                "</head>\n" + "<body>";
        for (Student student: studentList){
            str+= "<div>"+student.toString()+ "</div>";
        }
        return str +"<p><a href=\"http://localhost:63342/aboba2/src/main/Mainhtml.html?_ijt=ncrl02v1ok1r7uuuus3rcqcic9\">На главную</a></p></body>";
    }

    @GET
    @Path("/show-stud")
    @Produces("text/html")
    public String show_stud(){ //вывод студентов
        StudentService studentService = new StudentService();
        List<Student> studentList = studentService.findAllStudent();
        String str = "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Student</title>\n" +
                "</head>\n" + "<body>";
        for (Student student: studentList){
            str+= "<div>"+student.toString()+ "</div>";
        }
        return str +"<p><a href=\"http://localhost:63342/aboba2/src/main/Mainhtml.html?_ijt=ncrl02v1ok1r7uuuus3rcqcic9\">На главную</a></p></body>";
    }
    @POST
    @Path("/hardquerylike")
    @Produces("text/html")
    public String hardquerylike(@FormParam("name_like") String name_like){ //поиск студентов по имени по подстроке
        StudentService studentService = new StudentService();
        List<Student> studentList = studentService.HardQueryLike(name_like);
        String str = "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Student</title>\n" +
                "</head>\n" + "<body>";
        for (Student student: studentList){
            str+= "<div>"+student.toString()+ "</div>";
        }
        return str +"<p><a href=\"http://localhost:63342/aboba2/src/main/Mainhtml.html?_ijt=ncrl02v1ok1r7uuuus3rcqcic9\">На главную</a></p></body>";
    }
    @GET
    @Path("/sort-student")
    @Produces("text/html")
    public String sort_student(){ //сортировка студентов
        StudentService studentService = new StudentService();
        List<Student> students = studentService.sort_student();
        String str = "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Gruppa</title>\n" +
                "</head>\n" + "<body>";
        for (Student student: students){
            str+= "<div>"+student.toString()+ "</div>";
        }
        return str +"<p><a href=\"http://localhost:63342/aboba2/src/main/Mainhtml.html?_ijt=ncrl02v1ok1r7uuuus3rcqcic9\">На главную</a></p></body>";
    }
    @GET
    @Path("/pag-student")
    @Produces("text/html")
    public String pagin(){ //пагинация. расчет страниц
        StudentService studentService = new StudentService();
        List<Student> students = studentService.findAllStudent();
        String string="";
        int size=students.size();
        int pages=(size/count);
        if (size%count>0){
            pages++;
        }
        if (count>size){
            string += "<p><a href = \"pag-student/" + 1 + "\">" + 1 + "</a></p>";
        } else {
            for (int i = 1; i <= pages; i++) {
                string += "<p><a href = \"pag-student/" + i + "\">" + i + "</a></p>";
            }
        }
        return string;
    }
    @GET
    @Produces("text/html")
    @Path("/pag-student/{c}")
    public String paginpage(@PathParam("c") int c, String args[]) { //пагинация. постраничный вывод

        StudentService studentService = new StudentService();
        List<Student> students = studentService.findAllStudent();
        int size = students.size();
        String html = "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Cities</title>\n" +
                "</head>\n" + "<body>";
        if (c == 1) {
            for (int i = 0; i < count; i++) {
                if (i >= size) {
                    break;
                }
                html += "<div>" + students.get(i).toString() + "</div>";
            }
        } else {
            for (int i = ((c - 1) * count); i < (c * count); i++) {
                if (i >= size) {
                    break;
                } else {
                    html += "<div>" + students.get(i).toString() + "</div>";
                }
            }
        }
        return html + "</body>";
    }
}

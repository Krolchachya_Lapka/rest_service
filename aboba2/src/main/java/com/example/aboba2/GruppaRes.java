package com.example.aboba2;
import models.Faculty;
import models.Gruppa;
import models.Student;
import services.FacultyService;
import services.GroupService;
import services.StudentService;

import javax.ws.rs.*;
import java.util.List;
@Path("/gruppa")
public class GruppaRes {
    public static int count=5;
    @GET
    @Path("/show-gruppa")
    @Produces("text/html")
    public String show_gruppa(){ //вывод таблицы групп
        GroupService groupService = new GroupService();
        List<Gruppa> gruppaList = groupService.findAllGroup();
        String str = "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Gruppa</title>\n" +
                "</head>\n" + "<body>";
        for (Gruppa gruppa: gruppaList){
            str+= "<div>"+gruppa.toString()+ "</div>";
        }
        return str +"<p><a href=\"http://localhost:63342/aboba2/src/main/Mainhtml.html?_ijt=ncrl02v1ok1r7uuuus3rcqcic9\">На главную</a></p></body>";
    }
    @GET
    @Path("/sort-gruppa")
    @Produces("text/html")
    public String sort_gruppa(){ //сортировка группы
        GroupService groupService = new GroupService();
        List<Gruppa> gruppaList = groupService.sort_gruppa();
        String str = "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Gruppa</title>\n" +
                "</head>\n" + "<body>";
        for (Gruppa gruppa: gruppaList){
            str+= "<div>"+gruppa.toString()+ "</div>";
        }
        return str +"<p><a href=\"http://localhost:63342/aboba2/src/main/Mainhtml.html?_ijt=ncrl02v1ok1r7uuuus3rcqcic9\">На главную</a></p></body>";
    }
    @POST
    @Path("/add-gruppa")
    @Produces("text/html")
    public String add_gruppa(@FormParam("name_gruppa") String name_gruppa, @FormParam("id_faculty") int id_faculty){ //добвление группы
        FacultyService facultyService = new FacultyService();
        GroupService groupService = new GroupService();
        Gruppa gruppa = new Gruppa(name_gruppa);
        Faculty faculty = facultyService.findFaculty(id_faculty);
        gruppa.setFaculty(faculty);
        groupService.saveGroup(gruppa);
        return "DELO SDELANO";
    }
    @POST
    @Path("/delete-gruppa")
    @Produces("text/html")
    public String delete_gruppa(@FormParam("id_gruppa") int id_gruppa){ //удаление группы
        GroupService groupService = new GroupService();
        Gruppa gruppa = groupService.findGroup(id_gruppa);
        groupService.deleteGroup(gruppa);
        return "DELO SDELANO";
    }
    @POST
    @Path("/update-gruppa")
    @Produces("text/html")
    public String update_gruppa(@FormParam("id_gruppa") int id_gruppa,@FormParam("name_gruppa") String name_gruppa,@FormParam("id_faculty") int id_faculty){ //обновление группы

        GroupService groupService = new GroupService();
        Gruppa gruppa = groupService.findGroup(id_gruppa);
        gruppa.setName(name_gruppa);
        FacultyService facultyService = new FacultyService();
        Faculty faculty = facultyService.findFaculty(id_faculty);
        gruppa.setFaculty(faculty);
        groupService.updateGroup(gruppa);
        return "DELO SDELANO";
    }
    @GET
    @Path("/pag-gruppa")
    @Produces("text/html")
    public String pagin(){ //пагинация. рассчет страниц
        GroupService groupService = new GroupService();
        List<Gruppa> gruppas = groupService.findAllGroup();
        String string="";
        int size=gruppas.size();
        int pages=(size/count);
        if (size%count>0){
            pages++;
        }
        if (count>size){
            string += "<p><a href = \"pag-gruppa/" + 1 + "\">" + 1 + "</a></p>";
        } else {
            for (int i = 1; i <= pages; i++) {
                string += "<p><a href = \"pag-gruppa/" + i + "\">" + i + "</a></p>";
            }
        }
        return string;
    }
    @GET
    @Produces("text/html")
    @Path("/pag-gruppa/{c}")
    public String paginpage(@PathParam("c") int c, String args[]) { //пагинация. постраничный вывод

        GroupService groupService = new GroupService();
        List<Gruppa> gruppas = groupService.findAllGroup();
        int size = gruppas.size();
        String html = "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Cities</title>\n" +
                "</head>\n" + "<body>";
        if (c == 1) {
            for (int i = 0; i < count; i++) {
                if (i >= size) {
                    break;
                }
                html += "<div>" + gruppas.get(i).toString() + "</div>";
            }
        } else {
            for (int i = ((c - 1) * count); i < (c * count); i++) {
                if (i >= size) {
                    break;
                } else {
                    html += "<div>" + gruppas.get(i).toString() + "</div>";
                }
            }
        }
        return html + "</body>";
    }
    @POST
    @Path("/hardquery")
    @Produces("text/html")
    public String hardquery(@FormParam("name_faculty") String name_faculty){ // поиск групп по факультету
        GroupService groupService = new GroupService();
        List<Gruppa> gruppas = groupService.HardQueryWhere(name_faculty);
        String str = "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Student</title>\n" +
                "</head>\n" + "<body>";
        for (Gruppa gruppa: gruppas){
            str+= "<div>"+gruppa.toString()+ "</div>";
        }
        return str +"<p><a href=\"http://localhost:63342/aboba2/src/main/Mainhtml.html?_ijt=ncrl02v1ok1r7uuuus3rcqcic9\">На главную</a></p></body>";
    }
    @POST
    @Path("/hardquerylike")
    @Produces("text/html")
    public String hardquerylike(@FormParam("name_like") String name_like){ //поиск по подстроке
        GroupService groupService = new GroupService();
        List<Gruppa> gruppas = groupService.HardQueryLike(name_like);
        String str = "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Student</title>\n" +
                "</head>\n" + "<body>";
        for (Gruppa gruppa: gruppas){
            str+= "<div>"+gruppa.toString()+ "</div>";
        }
        return str +"<p><a href=\"http://localhost:63342/aboba2/src/main/Mainhtml.html?_ijt=ncrl02v1ok1r7uuuus3rcqcic9\">На главную</a></p></body>";
    }
}

package com.example.aboba2;

import models.Faculty;
import models.Gruppa;
import models.Student;
import services.FacultyService;
import services.GroupService;
import services.StudentService;

import javax.ws.rs.*;
import java.util.List;
@Path("/faculty")
public class FacultyRes {
    public static int count=5;
    @GET
    @Path("/show-faculty")
    @Produces("text/html")
    public String show_faculty(){ //вывод таблицы факультетов
        FacultyService facultyService = new FacultyService();
        List<Faculty> facultyList = facultyService.findAllFaculty();
        String str = "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Faculty</title>\n" +
                "</head>\n" + "<body>";
        for (Faculty faculty:facultyList){
            str+= "<div>"+faculty.toString()+ "</div>";
        }
        return str +"<p><a href=\"http://localhost:63342/aboba2/src/main/Mainhtml.html?_ijt=ncrl02v1ok1r7uuuus3rcqcic9\">На главную</a></p></body>";
    }
    @POST
    @Path("/add-faculty")
    @Produces("text/html")
    public String add_faculty(@FormParam("name_faculty") String name_faculty){ //добваление факультета
        try {
            FacultyService facultyService = new FacultyService();
            Faculty faculty = new Faculty(name_faculty);
            facultyService.saveFaculty(faculty);
            return "DELO SDELANO";
        } catch (Exception e){
            return "Некорректные данные";
        }

    }
    @POST
    @Path("/delete-faculty")
    @Produces("text/html")
    public String delete_faculty(@FormParam("id_faculty") int id_faculty){ //удаление факультета
        try {
            FacultyService facultyService = new FacultyService();
            Faculty faculty= facultyService.findFaculty(id_faculty);
            facultyService.deleteFaculty(faculty);
            return "DELO SDELANO";
        } catch (Exception e){
            return "Некорректные данные";
        }

    }
    @POST
    @Path("/update-faculty")
    @Produces("text/html")
    public String update_faculty(@FormParam("id_faculty") int id_faculty,@FormParam("name_faculty") String name_faculty){ //обновление факультета
        try {
            FacultyService facultyService = new FacultyService();
            Faculty faculty = facultyService.findFaculty(id_faculty);
            faculty.setName(name_faculty);
            facultyService.updateFaculty(faculty);
            return "DELO SDELANO";
        } catch (Exception e){
            return "Некорректные данные";
        }

    }
    @GET
    @Path("/sort-faculty")
    @Produces("text/html")
    public String sort_faculty(){ //сортировка факультетов
        FacultyService facultyService = new FacultyService();
        List<Faculty> facultyList = facultyService.sort_faculty();
        String str = "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Gruppa</title>\n" +
                "</head>\n" + "<body>";
        for (Faculty faculty: facultyList){
            str+= "<div>"+faculty.toString()+ "</div>";
        }
        return str +"<p><a href=\"http://localhost:63342/aboba2/src/main/Mainhtml.html?_ijt=ncrl02v1ok1r7uuuus3rcqcic9\">На главную</a></p></body>";
    }
    @POST
    @Path("/hardquerylike")
    @Produces("text/html")
    public String hardquerylike(@FormParam("name_like") String name_like){ //поиск факультетов по имени по подстроке
        FacultyService facultyService = new FacultyService();
        List<Faculty> faculties = facultyService.HardQueryLike(name_like);
        String str = "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Student</title>\n" +
                "</head>\n" + "<body>";
        for (Faculty faculty: faculties){
            str+= "<div>"+faculty.toString()+ "</div>";
        }
        return str +"<p><a href=\"http://localhost:63342/aboba2/src/main/Mainhtml.html?_ijt=ncrl02v1ok1r7uuuus3rcqcic9\">На главную</a></p></body>";
    }
    @GET
    @Path("/pag-faculty")
    @Produces("text/html")
    public String pagin(){ //пагинация. рассчет страниц
        FacultyService facultyService = new FacultyService();
        List<Faculty> facultyList = facultyService.findAllFaculty();
        String string="";
        int size=facultyList.size();
        int pages=(size/count);
        if (size%count>0){
            pages++;
        }
        if (count>size){
            string += "<p><a href = \"pag-faculty/" + 1 + "\">" + 1 + "</a></p>";
        } else {
            for (int i = 1; i <= pages; i++) {
                string += "<p><a href = \"pag-faculty/" + i + "\">" + i + "</a></p>";
            }

        }
        return string;
    }
    @GET
    @Produces("text/html")
    @Path("/pag-faculty/{c}")
    public String paginpage(@PathParam("c") int c, String args[]) { //пагинация. постраничный вывод

        FacultyService facultyService = new FacultyService();
        List<Faculty> faculties = facultyService.findAllFaculty();
        int size = faculties.size();
        String html = "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Cities</title>\n" +
                "</head>\n" + "<body>";
        if (c == 1) {
            for (int i = 0; i < count; i++) {
                if (i >= size) {
                    break;
                }
                html += "<div>" + faculties.get(i).toString() + "</div>";
            }
        } else {
            for (int i = ((c - 1) * count); i < (c * count); i++) {
                if (i >= size) {
                    break;
                } else {
                    html += "<div>" + faculties.get(i).toString() + "</div>";
                }
            }
        }
        return html + "</body>";
    }
}

package models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
@Entity
@Table (name = "faculty")
public class Faculty {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "id")
    private int id;
    private String name;
    @OneToMany(mappedBy = "faculty", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Gruppa> gruppas;

    public Faculty() {
    }

    public Faculty(String name) {
        this.name = name;
        gruppas = new ArrayList<>();
    }
    public void setId(int value) {this.id=value;}
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Gruppa> getGroups() {
        return gruppas;
    }

    public void setGroups(List<Gruppa> gruppas) {
        this.gruppas = gruppas;
    }

    @Override
    public String toString() {
        return
                "id=" + id +
                        ", Название=" + name ;
}}

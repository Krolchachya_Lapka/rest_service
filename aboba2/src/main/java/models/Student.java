package models;
import javax.persistence.*;

@Entity
@Table (name = "student")
public class Student {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private int id;
        private String Full_name;
   // fetch = FetchType.LAZY
        @ManyToOne()
        @JoinColumn(name = "group_id")
        private Gruppa gruppa;
        public Student() {
        }

        public Student(String name) {
            this.Full_name = name;
        }

        public int getId() {
            return id;
        }

        public String getFull_name() {
            return Full_name;
        }

        public void setFull_name(String name) {
            this.Full_name = name;
        }

        public Gruppa getGroup() {
            return this.gruppa;
        }

        public void setGroup(Gruppa gruppa) {
            this.gruppa = gruppa;
        }

        @Override
        public String toString() {
            return
                    "id=" + id +
                    ", Имя=" + Full_name + ", Группа="+this.gruppa.getName();
}}

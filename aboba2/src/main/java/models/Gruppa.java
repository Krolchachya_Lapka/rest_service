package models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
@Entity
@Table (name = "gruppa")
public class Gruppa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    @OneToMany(mappedBy = "gruppa", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Student> students;
    @ManyToOne()
    @JoinColumn(name = "fac_id")
    private Faculty faculty;

    public Gruppa() {
    }

    public Gruppa(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public List<Student> getStudents() {
//        return students;
//    }
//
//    public void setStudents(List<Student> students) {
//        this.students = students;
//    }
//    public Faculty getFaculty() {
//        return faculty;
 //   }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    @Override
    public String toString() {
        return
                "id=" + id +
                ", Название=" + name + ", Факультет="+faculty.getName();
}}

package services;
import dao.FacultyDao;
import models.Faculty;
import models.Gruppa;

import java.util.List;
public class FacultyService {
    private FacultyDao facultyDao = new FacultyDao();

    public FacultyService() {
    }

    public Faculty findFaculty(int id) {
        return facultyDao.findById(id);
    }

    public void saveFaculty(Faculty faculty) {
        facultyDao.save(faculty);
    }

    public void deleteFaculty(Faculty faculty) {
        facultyDao.delete(faculty);
    }

    public void updateFaculty(Faculty faculty) {
        facultyDao.update(faculty);
    }

    public List<Faculty> findAllFaculty() {
        return facultyDao.findAll();
    }
    public List<Faculty> sort_faculty() {
        return facultyDao.sort_faculty();
    }
    public List<Faculty> HardQueryLike(String str){return  facultyDao.HardQueryLike(str);}
}

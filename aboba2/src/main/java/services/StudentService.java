package services;
import dao.StudentDao;
import models.Faculty;
import models.Student;

import java.util.List;
public class StudentService {
    private StudentDao studentDao = new StudentDao();

    public StudentService() {
    }

    public Student findStudent(int id) {
        return studentDao.findById(id);
    }

    public void saveStudent(Student student) {
        studentDao.save(student);
    }

    public void deleteStudent(Student student) {
        studentDao.delete(student);
    }

    public void updateStudent(Student student) {
        studentDao.update(student);
    }

    public List<Student> findAllStudent() {
        return studentDao.findAll();
    }
    public List<Student> HardQueryWhere(String str){return  studentDao.HardQueryWhere(str);}
    public List<Student> HardQueryLike(String str){return  studentDao.HardQueryLike(str);}
    public List<Student> sort_student() {
        return studentDao.sort_student();
    }
}

package services;
import dao.GroupDao;
import models.Gruppa;
import models.Student;

import java.util.List;
public class GroupService {
    private GroupDao groupDao = new GroupDao();

    public GroupService() {
    }

    public Gruppa findGroup(int id) {
        return groupDao.findById(id);
    }

    public void saveGroup(Gruppa gruppa) {
        groupDao.save(gruppa);
    }

    public void deleteGroup(Gruppa gruppa) {
        groupDao.delete(gruppa);
    }

    public void updateGroup(Gruppa gruppa) {
        groupDao.update(gruppa);
    }

    public List<Gruppa> findAllGroup() {
        return groupDao.findAll();
    }
    public List<Gruppa> sort_gruppa() {
        return groupDao.sort_gruppa();
    }
    public List<Gruppa> HardQueryWhere(String str){return  groupDao.HardQueryWhere(str);}
    public List<Gruppa> HardQueryLike(String str){return  groupDao.HardQueryLike(str);}
}
